const axios=require('axios')
const logs4js=require('log4js')
 const RandomNumber=(minimo, maximo)=> {
    return Math.floor(Math.random() * (maximo - minimo + 1)) + minimo;
}

 const StatusCocina=(status)=> {
    switch (status) {
        case 1:
            return "Preparando"
        case 2:
            return "Entregado al repartidor"
        case 3:
            return "Entregado al cliente"
        default:
            return "Error"
    }
}

 const StatusRepartidor=(status)=> {
    switch (status) {
        case 1:
            return "Esperando a Cocina"
        case 2:
            return "En Ruta"
        case 3:
            return "Entregado"
        default:
            return "Error"
    }
}

 const buscarPedido=(Arreglo, key, value)=> {
    for (var a = 0; a < Arreglo.length; a++) {
        
        if (String(Arreglo[a][key]) === String(value)) {
            return a;
        }
    }
    return -1;
}

 const  generarPeticion=async(ruta,datos,tipo)=>{
    switch(tipo){
        case "POST":
           return await axios.post(ruta,datos).then((respuesta)=>{
                return respuesta
            })
        case "GET":
           return await axios.get(ruta).then((respuesta)=>{
                return respuesta
            })
        default :
            return "Error"
    }
}

logs4js.configure({
    appenders: {
      file: { type: 'file', filename: '../salidas.txt' },
      console: { type: 'console' }
    },
    categories: {
      default: { appenders: ['file', 'console'], level: 'info' }
    }
  });
  
  // Obtener el logger
  const impresor = logs4js.getLogger();




module.exports={
    RandomNumber,
    StatusCocina,
    StatusRepartidor,
    buscarPedido,
    generarPeticion,
    impresor
}