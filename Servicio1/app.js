const express = require('express')
const bodyParser=require('body-parser')
const router = require( './routes')
const app=express()

app.set('port',1563)
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(router)

app.listen(app.get('port'),()=>{
    console.log("SERVIDOR SERVICIO 1 EN: 1563")
})