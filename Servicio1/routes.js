const {Router} = require( 'express')
const { generarPeticion, impresor } = require('../Functions/Numbers')

const router=Router()

router.get('/', (req, res) => {
    
    console.log("123")
    res.status(200).send('Server 1 running!')
})

router.get('/generarOrden', async(req, res) => {
    console.log("123")
    let status=await generarPeticion("http://localhost:2013/recibirPedido",null,"POST")
    let generarPedidoRepartidor=await generarPeticion("http://localhost:4500/recibirPedidoCocina",status.data,"POST")
    if(generarPedidoRepartidor.status===200){
        impresor.info("Respuesta desde Servicio 1: Exito 200 "+status.data)
        return res.status(200).send(generarPedidoRepartidor.data)
    }else{
        impresor.info("Respuesta desde Servicio 1: Error 400 "+status.data)
        return res.status(400).send(generarPedidoRepartidor.data)
    }
   
})

router.get('/getStatusCocina/:NoOrder', async(req, res) => {
    const{NoOrder}= req.params
    let status=await generarPeticion("http://localhost:2013/statusPedido/"+NoOrder,null,"GET")
    if(status.status===200){
        impresor.info("Respuesta desde Servicio 1: Exito 200 "+status.data)
        return res.status(200).send(status.data)
    }else{
        impresor.info("Respuesta desde Servicio 1: Error 400 "+status.data)
        return res.status(400).send(status.data)
    }
   
})

router.get('/getStatusRepartidor/:NoOrder', async(req, res) => {
    const{NoOrder}= req.params
    let status=await generarPeticion("http://localhost:4500/statusPedido/"+NoOrder,null,"GET")
    if(status.status===200){
        impresor.info("Respuesta desde Servicio 1: Exito 200 "+status.data)
        return res.status(200).send(status.data)
    }else{
        impresor.info("Respuesta desde Servicio 1: Error 400 "+status.data)
        return res.status(400).send(status.data)
    }
   
})

router.get('/envioRepartidor/:NoOrder', async(req, res) => {
    const{NoOrder}= req.params
    let status=await generarPeticion("http://localhost:2013/envioRepartidor/",{NoOrder:NoOrder},"POST")
    let generarPedidoRepartidor=await generarPeticion("http://localhost:4500/recibirPedidoCocina",status.data,"POST")
    if(generarPedidoRepartidor.status===200){
        impresor.info("Respuesta desde Servicio 1: Exito 200 "+generarPedidoRepartidor.data)
        return res.status(200).send(generarPedidoRepartidor.data)
    }else{
        impresor.info("Respuesta desde Servicio 1: Error 400 "+generarPedidoRepartidor.data)
        return res.status(400).send(generarPedidoRepartidor.data)
    }
   
})

router.get('/entregaCliente/:NoOrder', async(req, res) => {
    const { NoOrder } = req.params

    let entregaCliente=await generarPeticion("http://localhost:4500/entregaCliente",{NoOrder:NoOrder},"POST")
    if(entregaCliente.status===200){
        
        impresor.info('Respuesta desde Servicio 3: Exito 200 '+entregaCliente.data)
        return res.status(200).send(entregaCliente.data)
    }else{
        impresor.info('Respuesta desde Servicio 3: Error 400 '+entregaCliente.data)
        return res.status(400).send(entregaCliente.data)
    }
    
})


module.exports= router