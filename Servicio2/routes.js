const express = require('express')
const { RandomNumber, StatusCocina, buscarPedido, generarPeticion, impresor } = require('../Functions/Numbers')

const router = express()
let pedidos = []
router.get('/', (req, res) => {
    res.send('Server 2 running!')
})

router.post('/recibirPedido', async (req, res) => {
    let temporal = {
        NoOrder: RandomNumber(1000, 9999),
        StatusCocina: StatusCocina(1)
    }
    let index = buscarPedido(pedidos, 'NoOrder', temporal.NoOrder)
    if (index !== -1) {
        pedidos[index].StatusCocina = StatusCocina(1)
        impresor.info('Respuesta desde Servicio 2: Exito 200 ' + `El pedido fue recepcionado con exito NoOrder: ${temporal.NoOrder}`)
        return res.status(200).send({ NoOrder: temporal.NoOrder, Status: 1 })
    } else {
        pedidos.push(temporal)
        impresor.info('Respuesta desde Servicio 2: Exito 200 ' + `El pedido fue recepcionado con exito NoOrder: ${temporal.NoOrder}`)
        return res.status(200).send({ NoOrder: temporal.NoOrder, Status: 1 })
    }



})

router.get('/statusPedido/:NoOrder', async (req, res) => {
    const { NoOrder } = req.params

    let index = buscarPedido(pedidos, 'NoOrder', NoOrder)
    if (index !== -1) {
        impresor.info('Respuesta desde Servicio 2: Exito 200 ' + `El estado del pedido ${NoOrder} es ` + pedidos[index].StatusCocina)
        return res.status(200).send(`El estado del pedido ${NoOrder} es ` + pedidos[index].StatusCocina)
    } else {
        impresor.info("Respuesta desde Servicio 2: Error 400 " + `El pedido ${NoOrder} No existe`)
        return res.status(400).send(`El pedido ${NoOrder} No existe`)
    }

})

router.post('/envioRepartidor', async (req, res) => {
    const { NoOrder } = req.body

    let index = buscarPedido(pedidos, 'NoOrder', NoOrder)
    if (index !== -1) {
        pedidos[index].StatusCocina = StatusCocina(2)
        impresor.info('Respuesta desde Servicio 2: Exito 200 ' + `El pedido ${NoOrder} fue entregado al repartidor`)
        return res.status(200).send({ NoOrder: NoOrder, Status: 2 })

    } else {
        impresor.info("Respuesta desde Servicio 2: Error 400 " + `El pedido ${NoOrder} No existe`)
        return res.status(400).send({ NoOrder: NoOrder, Status: 2 })
    }

})




module.exports = router