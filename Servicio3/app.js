const express = require('express')
const router = require( './routes')
const bodyParser=require('body-parser')
const app=express()

app.set('port',4500)
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(router)

app.listen(app.get('port'),()=>{
    console.log("SERVIDOR SERVICIO 3 EN: 4500")
})