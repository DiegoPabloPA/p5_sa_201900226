const express = require( 'express')
const { StatusRepartidor, buscarPedido, impresor } = require('../Functions/Numbers')
const router=express()

router.get('/', (req, res) => {
    res.send('Server 3 running!')
})
let pedidos=[]
router.post('/recibirPedidoCocina', (req, res) => {
    const{NoOrder,Status}=req.body
    let temporal={
        NoOrder: Number(NoOrder),
        StatusRepartidor: StatusRepartidor(Number(Status))
    }
    
    let index=buscarPedido(pedidos,'NoOrder',NoOrder)
    if(index!==-1){
        pedidos[index].StatusRepartidor=temporal.StatusRepartidor
        impresor.info('Respuesta desde Servicio 3: Exito 200 '+`El pedido fue registrado en el servicio de repartidores: ${temporal.NoOrder} Estado: ${temporal.StatusRepartidor}`)
        return res.status(200).send(`El pedido fue registrado NoOrder: ${temporal.NoOrder}`)
    }else{
        pedidos.push(temporal)
        impresor.info('Respuesta desde Servicio 3: Exito 200 '+`El pedido fue registrado en el servicio de repartidores: ${temporal.NoOrder} Estado: ${temporal.StatusRepartidor}`)
        return res.status(200).send(`El pedido fue registrado NoOrder: ${temporal.NoOrder}`)
    }
    
})

router.get('/statusPedido/:NoOrder', async(req, res) => {
    const { NoOrder } = req.params

    let index=buscarPedido(pedidos,'NoOrder',NoOrder)
    if(index!==-1){
        impresor.info('Respuesta desde Servicio 3: Exito 200 '+`El estado del pedido ${NoOrder} es `+pedidos[index].StatusRepartidor)
        return res.status(200).send(`El estado del pedido ${NoOrder} es `+pedidos[index].StatusRepartidor)
    }else{
        impresor.info('Respuesta desde Servicio 3: Error 400 '+`El pedido ${NoOrder} No existe`)
        return res.status(400).send(`El pedido ${NoOrder} No existe`)
    }
    
})

router.post('/entregaCliente', async(req, res) => {
    const { NoOrder } = req.body

    let index=buscarPedido(pedidos,'NoOrder',NoOrder)
    if(index!==-1){
        pedidos[index].StatusRepartidor=StatusRepartidor(3)
        impresor.info('Respuesta desde Servicio 3: Exito 200 '+`El pedido ${NoOrder} fue entregado al cliente `)
        return res.status(200).send(`El pedido ${NoOrder} fue entregado al cliente `)
    }else{
        impresor.info('Respuesta desde Servicio 3: Error 400 '+`El pedido ${NoOrder} No existe`)
        return res.status(400).send(`El pedido ${NoOrder} No existe`)
    }
    
})



module.exports= router