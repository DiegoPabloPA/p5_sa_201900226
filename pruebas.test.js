const request = require('supertest')


const { generarPeticion } = require('./Functions/Numbers');
const server="http://localhost:1563"
describe('Test Practica 6', () => {

    it('Servidor Iniciado', async () => {
        const response = await generarPeticion(server+'/',null,'GET');
        expect(response.status).toBe(200);
      });


      it('Generar Orden', async () => {
        const response = await generarPeticion(server+'/generarOrden',null,'GET');
   
        expect(response.status).toBe(200);
      });

      it('Envio Orden Repartidor', async () => {
        const response = await generarPeticion(server+'/envioRepartidor/6280',null,'GET');
        console.log(response)
        expect(response.status).toBe(200);
      });
     
 
});
